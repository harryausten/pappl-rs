#![allow(non_camel_case_types, non_snake_case, non_upper_case_globals)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

#[cfg(test)]
mod test {
    use crate::{papplDeviceList, pappl_devtype_e_PAPPL_DEVTYPE_ALL};
    use std::{
        ffi::CStr,
        os::raw::{c_char, c_void},
        ptr,
    };

    fn print_cstr(name: &str, value: *const c_char) {
        if value.is_null() {
            return;
        }

        let value = unsafe { CStr::from_ptr(value) };
        println!("{} = {}", name, value.to_str().unwrap());
    }

    extern "C" fn print_device(
        device_info: *const c_char,
        device_uri: *const c_char,
        device_id: *const c_char,
        _data: *mut c_void,
    ) -> bool {
        print_cstr("device_info", device_info);
        print_cstr("device_uri", device_uri);
        print_cstr("device_id", device_id);
        true
    }

    #[test]
    fn test_list_devices() {
        unsafe {
            // TODO: Currently hangs for a few seconds and then returns false, why? Seems to
            // require Avahi.service? Can systemd-resolved not be used instead?
            assert!(!papplDeviceList(
                pappl_devtype_e_PAPPL_DEVTYPE_ALL,
                Some(print_device),
                ptr::null_mut(),
                None,
                ptr::null_mut(),
            ));
        }
    }
}
